/* buffer_and_average.c
 * 
 * This function implements a simple ring buffer which stores values and then averages all of them as the output.
 *
 * Added a reset input that when non-zero, sets the actual average size back to zero.
 * When zero, the actual avewrage size is allowed to increase
 * by one each sample until actual size is equal to the size input
 *
 * Adds a new output which is the current average size
 *
 * Authors: JCB
 * June 1 2016
 *
 */

#define MAX_SIZE 256

void buffer_and_average_reset(double *argin, int nargin, double *argout, int nargout){

    //Initialize buffer to zeros. Static ensures kept from call to call
    static double buffer[MAX_SIZE] = { 0 };
    static int current_pointer = 0;
    double data;
    int size_limit;
    static int current_size=0;
    double stride;
    static int cycle_counter = 0;
    double sum = 0;
    static double last_average = 0.0;
    int cycles_between_data = 0;
    int i;
    int reset;

    data = argin[0];
    size_limit = argin[1] - 1;
    stride = argin[2];
    reset = (int)argin[3];

    //Initialize number of cycles between taking data (input is in seconds)
    cycles_between_data = (int) (stride * FE_RATE);
    
    //Sanity check on buffer used
    if ( size_limit <= 0)
    {
        size_limit = 0;
    } 
    else if (size_limit >= MAX_SIZE)
    {
        size_limit = MAX_SIZE-1;
    }

    // since size_limit is an input, we have to account for the case
    // where size_limit is set to lower than current_limit
    if ( current_size > size_limit + 1 ) {
        current_size = size_limit;
    }

    // handle a reset pulse
    if (reset != 0) {
        current_size = 0;

        current_pointer = 0;
    }

    //Sanity check on how often record a value
    if (cycles_between_data <= 1)
    {
        cycles_between_data = 1;
    }

    //Increment cycle count
    cycle_counter++;
    if (cycle_counter >= cycles_between_data) {
        cycle_counter = 0;
        buffer[current_pointer] = data;
        current_pointer++;
        if(current_size <= size_limit) {
          ++current_size;
        }
    }

    //Go back to beginning of Ring Buffer
    if ( current_pointer > size_limit )
    {
        current_pointer = 0;
    }

    //Average and output
    sum = 0;
    for (i=0;i < (current_size);i++)
    {
        sum += buffer[i];
    }
    if(current_size > 0) {
        last_average = sum / ((double)current_size);
    }
    else {
        // skip updating.  we don't have any good data. send last value.
    }

    argout[0] = last_average;
    argout[1] = current_size;
}

#undef MAX_SIZE
