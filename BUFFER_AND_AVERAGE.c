/* buffer_and_average.c
 * 
 * This function implements a simple ring buffer which stores values and then averages all of them as the output.
 *
 * Authors: JCB
 * June 1 2016
 *
 */

#define MAX_SIZE 256

void buffer_and_average(double *argin, int nargin, double *argout, int nargout){

    //Initialize buffer to zeros. Static ensures kept from call to call
    static double buffer[MAX_SIZE] = { 0 };
    static int current_pointer = 0;
    double data;
    int size;
    double stride;
    static int cycle_counter = 0;
    double sum = 0;
    int cycles_between_data = 0;
    int i;

    data = argin[0];
    size = argin[1] - 1;
    stride = argin[2];

    //Initialize number of cycles between taking data (input is in seconds)
    cycles_between_data = (int) (stride * FE_RATE);
    
    //Sanity check on buffer used
    if ( size <= 0)
    {
        size = 0;
    } 
    else if (size >= MAX_SIZE)
    {
        size = MAX_SIZE-1;
    }

    //Sanity check on how often record a value
    if (cycles_between_data <= 1)
    {
        cycles_between_data = 1;
    }

    //Increment cycle count
    cycle_counter++;
    if (cycle_counter >= cycles_between_data) {
        cycle_counter = 0;
        buffer[current_pointer] = data;
        current_pointer++;
    }

    //Go back to beginning of Ring Buffer
    if ( current_pointer > size ) 
    {
        current_pointer = 0;
    }

    //Average and output
    sum = 0;
    for (i=0;i < (size+1);i++)
    {
        sum += buffer[i];
    }
    argout[0] = sum/((double) size + 1.0);
}

#undef MAX_SIZE
