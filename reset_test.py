import x1banda_pybind as x1banda
import matplotlib.pyplot as plot
import numpy as np

mod = x1banda.create_instance()

rate_hz = mod.get_model_rate_Hz()
# set up input stream
# 30 seconds at 100
in_stream = [1.0] * 30 * rate_hz

# followed by 30 seconds at zero, then another 30 seconds at 100
in_stream += [0.0] * 30 * rate_hz + in_stream[:]

mod.set_adc_channel_generator(0,0,x1banda.CannedSignalGenerator(in_stream))

# setup the reset line.  go right after the second transition from 0 to 100
reset_stream = [0.0] * 90 * rate_hz
reset_stream[60*rate_hz] = 1.0

mod.set_adc_channel_generator(0,1,x1banda.CannedSignalGenerator(reset_stream))

mod.record_model_var("ORIG_AVG", rate_hz)
mod.record_model_var("RESET_AVG", rate_hz)
mod.record_model_var("RESET_SIZE", rate_hz)

# run for 90 sec
ticks = rate_hz * 90
time_s = np.array(range(ticks)) * (1/ rate_hz)
mod.run_model(ticks)


orig_avg = mod.get_recorded_var("ORIG_AVG")
reset_avg = mod.get_recorded_var("RESET_AVG")
reset_size = mod.get_recorded_var("RESET_SIZE")



fig1 = plot.figure(0)
plot.plot(time_s, orig_avg)
plot.plot(time_s, reset_avg)
plot.grid(True)
plot.legend(["Without reset", "With reset"])
plot.title("BUFFER_AND_AVERAGE with and without reset")
fig1.show()

fig2 = plot.figure(1)
plot.plot(time_s, reset_size)
plot.grid(True)
plot.title("Average size of reset part")
fig2.show()

input()
